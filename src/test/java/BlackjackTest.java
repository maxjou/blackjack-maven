import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;




public class BlackjackTest {
	
	Deck deck = new Deck();
	BlackJack bj = BlackJack.getInstance();

	@Test
	 void testScore0() {
		bj.playerHand = new ArrayList<>();
		bj.playerHand.add(new Card(Rank.ACE, Suit.H));
		bj.playerHand.add(new Card(Rank.TWO, Suit.S));
		assertEquals(Rank.ACE.getValue()+Rank.TWO.getValue() == 13, bj.score(bj.playerHand) == 13, "11+2=13");
		bj.playerHand.clear();
		bj.playerHand.add(new Card(Rank.NINE, Suit.H));
		bj.playerHand.add(new Card(Rank.TEN, Suit.S));
		assertEquals(Rank.NINE.getValue() + Rank.TEN.getValue() == 19, bj.score(bj.playerHand) == 19, "9+10=19");
		bj.playerHand.clear();
		bj.playerHand.add(new Card(Rank.TEN, Suit.H));
		bj.playerHand.add(new Card(Rank.JACK, Suit.S));
		assertEquals(Rank.TEN.getValue() + Rank.JACK.getValue() == 20, bj.score(bj.playerHand) == 20, "10+10=20");
		bj.playerHand.clear();
		bj.playerHand.add(new Card(Rank.QUEEN, Suit.H));
		bj.playerHand.add(new Card(Rank.KING, Suit.S));
		assertEquals(Rank.QUEEN.getValue() + Rank.KING.getValue() == 20, bj.score(bj.playerHand) == 20, "10+10=20");
	}

	@BeforeEach
	 void newArrayList() {
		bj.playerHand = new ArrayList<>();
		bj.dealerHand = new ArrayList<>();
	}

	@Test
	void testAces0() {
		bj.playerHand.add(new Card(Rank.ACE, Suit.C));
		bj.playerHand.add(new Card(Rank.ACE, Suit.H));
		bj.playerHand.add(new Card(Rank.ACE, Suit.S));
		bj.playerHand.add(new Card(Rank.JACK, Suit.C));
		assertTrue(bj.score(bj.playerHand) == 13, "1+1+1+10=13");
	}

	@Test

	 void TestAces1() {
		bj.playerHand.add(new Card(Rank.ACE, Suit.C));
		bj.playerHand.add(new Card(Rank.ACE, Suit.S));
		bj.playerHand.add(new Card(Rank.ACE, Suit.H));
		bj.playerHand.add(new Card(Rank.ACE, Suit.D));
		bj.playerHand.add(new Card(Rank.SEVEN, Suit.C));
		assertTrue(bj.score(bj.playerHand) == 21, "1+1+1+11+7=21");
	}

	@Test
	 void TestAces2() {
		bj.playerHand.add(new Card(Rank.ACE, Suit.C));
		bj.playerHand.add(new Card(Rank.ACE, Suit.S));
		bj.playerHand.add(new Card(Rank.ACE, Suit.D));
		bj.playerHand.add(new Card(Rank.ACE, Suit.H));
		bj.playerHand.add(new Card(Rank.QUEEN, Suit.C));
		assertTrue(bj.score(bj.playerHand) == 14, "1+1+1+1+10=14");
	}

	@Test
	 void testAces3() {
		// bj.playerHand = new ArrayList<>();
		bj.playerHand.add(new Card(Rank.ACE, Suit.C));
		bj.playerHand.add(new Card(Rank.ACE, Suit.S));
		bj.playerHand.add(new Card(Rank.JACK, Suit.C));
		assertTrue(bj.score(bj.playerHand) == 12, "1+1+10=12");
	}


	@Test
	 void testDeckSize() {
		assertTrue(deck.cards.size() == 52, "If there is 52 cards in Deck.");
	}

	@Test
	 void testAllCourtCardsValue() {
		
		assertTrue((Rank.JACK.getValue() + Rank.QUEEN.getValue() + Rank.KING.getValue() == 30), 
				"Court Cards have same value");
	}
	
	@ParameterizedTest
	@EnumSource(value = Suit.class, names = {"H","C","D","S"})
	void testEnumSuitNotNull(Suit suit) {
		assertNotNull(suit);
	}
	
//	@ParameterizedTest
//	@ValueSource(ints = {2,3,4,5,6,7,8,9,10,11})
//	 void testValueAllCardsInDeck(int n) {
//			for(Rank name : Rank.values()) {
//			bj.playerHand.add(new Card(name,Suit.C));
//			assertTrue(bj.playerHand.get(n).equals(name));
//		}	
//	}
	
//	@ParameterizedTest
//	@ValueSource(ints = {2,3,4,5,6,7,8,9,10,10,10,11})
//	 void testValueAllCardsInDeck(Rank value) {
//		assertEquals(true, new Card(value, Suit.C).equals(new Card(value,Suit.C)));
//	}
	
	@RepeatedTest(100)
	 void testDrawNotNull() {
		assertNotNull(deck.draw() != null, "Never draw a null card");
		assertFalse(deck.cards.size() > 52, "After 52 it will be false");
	}
	
	
	@RepeatedTest(100)
	 void testNeverDrawSameCard() {
		deck.shuffle();
		bj.playerHand.add(deck.draw());
		bj.dealerHand.add(deck.draw());
		assertNotEquals((bj.dealerHand).equals(bj.playerHand), (bj.dealerHand)!=(bj.playerHand),
				"Player and dealer never gets the same card");
		
	}
}
