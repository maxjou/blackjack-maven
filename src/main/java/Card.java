public class Card {
	private Rank name;
	private Suit suit;

	public Card() {
	}

	public Card(Rank name) {
		this.name = name;
		
	}
	public Card(Rank name, Suit suit) {
		this.name = name;
		this.suit = suit;

	}	

	public Rank setValue(Rank name) {
		this.name = name;
		return name;
	}

	public int getValue() {

		return name.getValue();

	}

	public String getSuit() {
		

		return suit.getSuit();
	}
	public String getName() {
		return name.getName();
	}
	@Override
	public String toString() {
		return "Card [name=" + name + ", suit=" + suit + "]";
	}

	@Override
	public boolean equals(Object object) {
		return ((Card)object).name == name && ((Card)object).suit == suit;
	}
}