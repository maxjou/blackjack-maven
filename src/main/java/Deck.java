import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

public class Deck {
	public List<Card> cards = new ArrayList<Card>();

	public Deck() {
		for (Suit suit : Suit.values()) {
			for (Rank name : Rank.values()) {
				cards.add(new Card(name, suit));

			}
		}
	}

	public Card draw() {

		cards.get(0);
		return cards.remove(0);
		
	}

	public void shuffle() {
		Collections.shuffle(cards);
	}

//	public static boolean testDeckSize() {
//		Deck deck = new Deck();
//		int numberOfCards = deck.cards.size();
//		return numberOfCards == 52;
//		// return new Deck().cards.size() == 52;
//	}
//
//	public static boolean testDraw() {
//		Deck deck = new Deck();
//		return deck.draw() != null;
//	}
//
//	public static boolean testDrawTwoCards() {
//		Deck deck = new Deck();
//		Card card0 = deck.draw();
//		Card card1 = deck.draw();
//		return card0.getValue() != card1.getValue() || card0.getSuit() != card1.getSuit();
//	}
//
//	public static boolean testDraw53Cards() {
//		Deck deck = new Deck();
//
//		for (int i = 0; i < 52; i++) {
//			if (deck.draw() == null) {
//				return false;
//			}
//		}
//		return deck.cards.isEmpty();
//	}
//
//	public static boolean testShuffle() {
//		Deck deck = new Deck();
//		testDeckSize();
//		int numberOfCards = deck.cards.size();
//		deck.shuffle();
//		int numberOfShuffle = deck.cards.size();
//		return numberOfCards == numberOfShuffle;
//	}
//
//	public static boolean testShuffleDraw() {
//		Deck deck = new Deck();
//		deck.draw();
//		testShuffle();
//		return testShuffle();
//
//	}
//
//	public static boolean testShuffleDoubles() {
//		Deck deck = new Deck();
//		deck.shuffle();
//		while (deck.cards.size() > 0) {
//			// s drar tills det inte finns kort kvar.
//			Card s = deck.draw();
//			//Jämför f med s för att se att det inte finns dubletter. 
//			for (Card f : deck.cards) {
//				if (s.equals(f)) {
//					return false;
//				}
//			}
//		}
//		return true;
//
//	}
//
//	
//	
	public static void main(String[] args) {
		Deck d1 = new Deck();
		// System.out.println(testDeckSize());
		// System.out.println(testDraw());
		// System.out.println(testDrawTwoCards());
		// System.out.println(testDraw53Cards());
		// System.out.println(testShuffle());
		// System.out.println(testShuffleDraw());
//		System.out.println(testShuffleDoubles());
		System.out.println(d1.draw());
	}
}