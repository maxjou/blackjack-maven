import java.util.ArrayList;
import java.util.List;

public class BlackJack {

	private static BlackJack instance;
	public List<Card> playerHand;
	public List<Card> dealerHand;
	Deck deck;

	private BlackJack() {

	}

	public static BlackJack getInstance() {
		if (instance == null) {
			instance = new BlackJack();
		}
		return instance;
	}

	public void hit() {

		playerHand.add(deck.draw());
		printHand(playerHand);
		if (score(playerHand) > 21) {
			System.out.println("Busted!\nDealer won!");
		} else if (score(playerHand) == 21) {
			System.out.println("BlackJack, you won!");
		}
	}

	public void stand() {
		printHand(playerHand);

		while (score(dealerHand) < score(playerHand)) {
			dealerHand.add(deck.draw());
			printHand(dealerHand);
			System.out.println("Dealer got: " + score(dealerHand));
		}
		if (score(dealerHand) > 21) {
//			printHand(dealerHand);
			System.out.println("You won with: " + score(playerHand));

		} else if (score(playerHand) > score(dealerHand)) {
//			printHand(dealerHand);
			System.out.print("You won with: " + score(playerHand));

		} else if (score(dealerHand) == score(playerHand) || score(dealerHand) > score(playerHand)) {
			printHand(dealerHand);
			System.out.println("Dealer won!");

		}

	}

	public void reset() {
		deck = new Deck();
		deck.shuffle();
		playerHand = new ArrayList<>();
		dealerHand = new ArrayList<>();
		playerHand.add(deck.draw());
		dealerHand.add(deck.draw());
		playerHand.add(deck.draw());
		printHand(playerHand);
		printHand(dealerHand);
		dealerHand.add(deck.draw());

	}

	public int score(List<Card> hand) {
		int score = 0;
		int aces= 0;
		for (Card s : hand) {
			if(s.getName().equals(Rank.ACE.getName())) {
				aces++;			
			}else {
				score += s.getValue();
			}	
		}
		if (aces > 0) {
			int maxScore = score + 11 + aces - 1;
			
			if (maxScore <= 21) {
				return maxScore;
			}
			return score + aces;
		}
		return score;
	}
	

	public void printHand(List<Card> hand) {
		if (hand == playerHand) {
			System.out.println("Player hand: ");
		} else if (hand == dealerHand) {
			System.out.println("Dealer hand: ");
		}
		int score = 0;
		for (Card s : hand) {
			score += s.getValue();

			System.out.printf("%s %s			Score: %s\n", s.getName(), s.getSuit(),score);
		}
	}
	
	public static boolean testScore0() {
		BlackJack blackjack = new BlackJack();
		blackjack.playerHand = new ArrayList<>();

		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.TWO, Suit.S));

		return blackjack.score(blackjack.playerHand) ==  13;
	}
	public static boolean testScore1() {
		BlackJack blackjack = new BlackJack();

		blackjack.playerHand.add(new Card(Rank.NINE, Suit.H));
		blackjack.dealerHand.add(new Card(Rank.EIGHT, Suit.S));

		return blackjack.score(blackjack.playerHand) ==  19;
	}
	public static boolean testScore2() {
		BlackJack blackjack = new BlackJack();

		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.dealerHand.add(new Card(Rank.EIGHT, Suit.S));

		return blackjack.score(blackjack.playerHand) ==  13;
	}
	public static boolean testScore3() {
		BlackJack blackjack = new BlackJack();

		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.dealerHand.add(new Card(Rank.EIGHT, Suit.S));

		return blackjack.score(blackjack.playerHand) ==  13;
	}
		
	public  boolean scoreTest() {
		Deck deck = new Deck();
		playerHand = new ArrayList<>();
		dealerHand = new ArrayList<>();
		
		for(int i = 0; i < 52; i++) {
			playerHand.add(deck.draw());
			dealerHand.add(deck.draw());
			if(score(playerHand) + score(dealerHand)<= 21) {
				return true;
			}
			
			playerHand.remove(deck.draw());
			dealerHand.remove(deck.draw());
		}
		return false;
	}
	public  boolean scoreTestAce() {
		Deck deck = new Deck();
		playerHand = new ArrayList<>();
		dealerHand = new ArrayList<>();
		
		for(int i = 0; i < 52; i++) {
			playerHand.add(deck.draw());
			dealerHand.add(deck.draw());
			if(score(playerHand) + score(dealerHand)<= 21) {
				return true;
			}
			
			playerHand.remove(deck.draw());
			dealerHand.remove(deck.draw());
		}
		return false;
	}
	
	
	public static void main(String[] args) {
		
		System.out.println(testScore0());
	}

}
